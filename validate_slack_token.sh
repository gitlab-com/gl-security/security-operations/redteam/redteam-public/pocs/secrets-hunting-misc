#! /bin/bash

# Pass the token as the first paramenter to determine validity.
# Errors can be checked here:  https://api.slack.com/methods/auth.test#errors

TOKEN=$1
curl --location --request POST 'https://slack.com/api/auth.test' --header 'Authorization: Bearer ${TOKEN}'
